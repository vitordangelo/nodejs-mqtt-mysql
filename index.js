const mqtt = require('mqtt')
const mysql = require('mysql')

const connection = mysql.createConnection({
  host: 'localhost',
  user: 'mqtt-mysql',
  password: 'mqtt-mysql',
  database: 'mqtt-mysql',
  port: 3308
})

const client = mqtt.connect('mqtt://iot.eclipse.org')

client.on('connect', function () {
  client.subscribe('v2tech')
})

function queryMysqlSave (data) {
  return new Promise(resolve => {
    connection.query('INSERT INTO data SET ?', data, (error, results) => {
      if (error) {
        console.log(error)
        return false
      }
      resolve(console.log(results))
    })
  })
}

client.on('message', function (topic, message) {
  let data = message.toString()
  let dataJson = JSON.parse(data)
  console.log(dataJson)
  mqttSave(dataJson)
})

async function mqttSave (data) {
  await queryMysqlSave(data)
}

mqttSave()
